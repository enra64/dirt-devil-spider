#include "behavior.hpp"

void Behavior::loop() {
  if (isBumperDepressed()) {
    onBumperHit();
  }

  readAbyssSensors(abyssSensorState);
  if (abyssSensorState.triggered()) {
    onAbyssDetected(abyssSensorState);
  }
}

bool Behavior::executingForcedReverse() {
    if (!inForcedReverse) {
        return false;
    }

    if ((millis() - forcedReverseStart) >= FORCED_REVERSE_MS) {
        inForcedReverse = false;
        return false;
    }

    leftForward(-1);
    rightForward(-1);

    return true;
}
void Behavior::forceReverse() {
    forcedReverseStart = millis();
    inForcedReverse = true;
}