/**
 * This is the io routines system test.
 *
 * beeps once at the beginning and then after every movement
 * forward
 * stop
 * backwards
 * right forward
 * right backward
 * left forward
 * left backward
 */
#include <s_shape.hpp>
#include <Arduino.h>
#include <io_routines.hpp>

SShape behavior;
long startTime;

void setup() {
  setupPinModes();
  startTime = millis();
}

void loop() {
  behavior.loop();
}
