#include "s_shape.hpp"

// drive forward until obstacle is hit
// turn left, backwards
// drive forward shortly
// turn left, backwards
// go to start
void SShape::loop() {
  Behavior::loop();

  if (executingForcedReverse()) {
    return;
  }

  switch (state) {
  case SShapeState::LongForward:
    leftForward(1);
    rightForward(1);
    break;
  case SShapeState::FirstTurn:
    if (turnBackwards(false, FIRST_TURN_MS, currentStateStart)) {
      changeState(SShapeState::ShortForward);
    }
    break;
  case SShapeState::ShortForward:
    if (forward(SHORT_FORWARD_MS, currentStateStart)) {
      changeState(SShapeState::ShortForward);
    }
    break;
  case SShapeState::SecondTurn:
    if (turnBackwards(false, SECOND_TURN_MS, currentStateStart)) {
      changeState(SShapeState::ShortForward);
    }
    break;
  }
}

void SShape::changeState(SShapeState newState) {
  state = newState;
  currentStateStart = millis();
}

void SShape::onBumperHit() {
  switch (state) {
  case SShapeState::LongForward:
    changeState(SShapeState::FirstTurn);
    break;
  case SShapeState::FirstTurn:
    forceReverse();
    break;
  case SShapeState::ShortForward:
    changeState(SShapeState::SecondTurn);
    break;
  case SShapeState::SecondTurn:
    forceReverse();
    break;
  }
}

void SShape::onAbyssDetected(const AbyssSensorState &state) { onBumperHit(); }

SShape::SShape() {
  changeState(SShapeState::LongForward);
  currentStateStart = millis();
}

BehaviorType SShape::getBehaviorType() {
  return BehaviorType::SShape;
}