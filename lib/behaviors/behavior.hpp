#ifndef BEHAVIOR_H
#define BEHAVIOR_H

#include "../io/io_routines.hpp"

enum class BehaviorType {SShape, RemoteControl};

class Behavior {
private:
  AbyssSensorState abyssSensorState;
  bool inForcedReverse = false;
  long forcedReverseStart;
  const long FORCED_REVERSE_MS = 250;

protected:
  bool executingForcedReverse();
  void forceReverse();

public:
  Behavior() {};
  virtual ~Behavior() {};
  virtual void loop();
  virtual void onBumperHit() = 0;
  virtual void onAbyssDetected(const AbyssSensorState &state) = 0;
  virtual BehaviorType getBehaviorType() = 0;
};

#endif