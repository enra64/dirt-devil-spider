#include "behavior_manager.hpp"

void BehaviorManager::loop() {
    if (currentBehaviorDuration() > MAX_BEHAVIOR_DURATION) {
        
    }
}

void BehaviorManager::engageManualDriving() {
// todo
}
void BehaviorManager::disengageManualDriving() {
// todo
}

BehaviorManager::BehaviorManager() {
    loadBehavior(std::unique_ptr<Behavior>(new SShape()));
}

void BehaviorManager::loadBehavior(std::unique_ptr<Behavior> newBehavior) {
    lastBehaviorStart = millis();
    currentBehavior = std::move(newBehavior);
}

long BehaviorManager::currentBehaviorDuration() {
    return millis() - lastBehaviorStart;
}