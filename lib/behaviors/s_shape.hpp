#ifndef S_SHAPE_H
#define S_SHAPE_H

#include "behavior.hpp"

enum class SShapeState {LongForward, FirstTurn, ShortForward, SecondTurn};

class SShape : public Behavior {
private:
  const long FIRST_TURN_MS = 400;
  const long SECOND_TURN_MS = 400;
  const long SHORT_FORWARD_MS = 2000;
  SShapeState state = SShapeState::LongForward;
  long currentStateStart;
  void changeState(SShapeState newState);
public:
  SShape();
  void loop();
  void onBumperHit();
  void onAbyssDetected(const AbyssSensorState& state);
  BehaviorType getBehaviorType();
};

#endif