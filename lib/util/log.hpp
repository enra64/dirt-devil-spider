#ifndef LOG_HPP
#define LOG_HPP

#include <Arduino.h>
#include <string>

/**
 * This is a simple wrapper for serial.print to be able to swap out the logging method
 */
void info(const std::string& content) {
    Serial.print(content.c_str);
}

void initializeLogging() {
    Serial.begin(9600);
}

#endif