/**
 * This is the io routines system test. 
 * 
 * beeps once at the beginning and then after every movement
 * forward
 * stop
 * backwards
 * right forward
 * right backward
 * left forward
 * left backward
 */
#include <io_routines.hpp>
#include <Arduino.h>

long startTime;
int testedFunction;

void beep() {
  setSpeaker(true);
  delay(300);
  setSpeaker(true);
}

void setup() {
  setupPinModes();
  beep();

  startTime = millis();
  testedFunction = 0;
}

void loop() {
  switch (testedFunction) {
  case 0:
    if (forward(1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  case 1:
    if (stop(1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  case 2:
    if (reverse(1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  case 3:
    if (turn(true, 1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  case 4:
    if (turnBackwards(true, 1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  case 5:
    if (turn(false, 1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  case 6:
    if (turnBackwards(false, 1000, startTime)) {
      testedFunction++;
      startTime = millis();
      beep();
    }
    break;
  default:
    setSpeaker(true);
    break;
  }
}
