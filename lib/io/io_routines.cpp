#include "io_routines.hpp"
#include <Arduino.h>



void leftForward(float power) {
  digitalWrite(as_int(Pins::LeftForward), power > 0);
  digitalWrite(as_int(Pins::LeftBackward), power < 0);
}

void rightForward(float power) {
  digitalWrite(as_int(Pins::RightForward), power > 0);
  digitalWrite(as_int(Pins::RightBackward), power < 0);
}

void drive(float left, float right) {
  leftForward(left);
  rightForward(right);
}

void vacuum(bool enable) { digitalWrite(as_int(Pins::Vacuum), enable); }

void readAbyssSensors(AbyssSensorState &abyssSensorState) {
  abyssSensorState.left = digitalRead(as_int(Pins::AbyssLeft));
  abyssSensorState.center = digitalRead(as_int(Pins::AbyssCenter));
  abyssSensorState.right = digitalRead(as_int(Pins::AbyssRight));
}

bool isVacuumHoleClear() { return digitalRead(as_int(Pins::VacuumHole)); }

bool isBumperDepressed() { return digitalRead(as_int(Pins::Bumper)); }

void setRedLed(bool enable) { digitalWrite(as_int(Pins::LedRed), enable); }

void setBlueLed(bool enable) { digitalWrite(as_int(Pins::LedBlue), enable); }

void setSpeaker(bool enable) { digitalWrite(as_int(Pins::Speaker), enable); }

/**
 * Returns false until we have reversed for fullDuration ms
 */
bool reverse(long duration, long startTime) {
  long currentTime = millis();
  if ((currentTime - duration) <= startTime) {
    return true;
  }

  leftForward(-1);
  rightForward(-1);

  return false;
}

/**
 * Returns false until we have reversed for fullDuration ms
 */
bool forward(long duration, long startTime) {
  long currentTime = millis();
  if ((currentTime - duration) <= startTime) {
    return true;
  }

  leftForward(1);
  rightForward(1);

  return false;
}

/**
 * Returns false until we have reversed for fullDuration ms
 */
bool stop(long duration, long startTime) {
  long currentTime = millis();
  if ((currentTime - duration) <= startTime) {
    return true;
  }

  leftForward(0);
  rightForward(0);

  return false;
}

/**
 * Returns false until we have turned for fullDuration ms
 */
bool turnBackwards(bool right, long duration, long startTime) {
  long currentTime = millis();
  if ((currentTime - duration) <= startTime) {
    return true;
  }

  leftForward(right ? 0 : -1);
  rightForward(right ? -1 : 0);

  return false;
}

/**
 * Returns false until we have turned for fullDuration ms
 */
bool turn(bool right, long duration, long startTime) {
  long currentTime = millis();
  if ((currentTime - duration) <= startTime) {
    return true;
  }

  leftForward(!right);
  rightForward(right);

  return false;
}

void setupPinModes() {
  pinMode(as_int(Pins::LeftForward), OUTPUT);
  pinMode(as_int(Pins::LeftBackward), OUTPUT);
  pinMode(as_int(Pins::RightForward), OUTPUT);
  pinMode(as_int(Pins::RightBackward), OUTPUT);
  pinMode(as_int(Pins::Vacuum), OUTPUT);
  pinMode(as_int(Pins::LedBlue), OUTPUT);
  pinMode(as_int(Pins::LedRed), OUTPUT);
  pinMode(as_int(Pins::Speaker), OUTPUT);
  digitalWrite(as_int(Pins::LeftForward), LOW);
  digitalWrite(as_int(Pins::LeftBackward), LOW);
  digitalWrite(as_int(Pins::RightForward), LOW);
  digitalWrite(as_int(Pins::RightBackward), LOW);
  digitalWrite(as_int(Pins::Vacuum), LOW);
  digitalWrite(as_int(Pins::LedBlue), LOW);
  digitalWrite(as_int(Pins::LedRed), LOW);

  pinMode(as_int(Pins::AbyssLeft), INPUT);
  pinMode(as_int(Pins::AbyssCenter), INPUT);
  pinMode(as_int(Pins::AbyssRight), INPUT);
  pinMode(as_int(Pins::VacuumHole), INPUT);
  pinMode(as_int(Pins::Bumper), INPUT_PULLUP);
}
