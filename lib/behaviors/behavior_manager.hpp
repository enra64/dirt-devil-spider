#ifndef BEHAVIOR_MANAGER_HPP
#define BEHAVIOR_MANAGER_HPP

#include "behavior.hpp"
#include "s_shape.hpp"

#include <Arduino.h>
#include <utility>
#include <memory>


class BehaviorManager {
  long lastBehaviorStart;
  const std::vector<BehaviorType> AVAILABLE_BEHAVIORS = {BehaviorType::SShape, BehaviorType::RemoteControl};
  std::unique_ptr<Behavior> currentBehavior;

  const long MAX_BEHAVIOR_DURATION = 60000;
  long currentBehaviorDuration();
  void loadBehavior(std::unique_ptr<Behavior> newBehavior);

public:
  void loop();
  void engageManualDriving();
  void disengageManualDriving();
  BehaviorManager();
};

#endif