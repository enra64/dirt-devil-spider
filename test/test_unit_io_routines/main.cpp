#include <Arduino.h>
#include <io_routines.hpp>
#include <unity.h>

// void setUp(void) {
// // set stuff up here
// }

// void tearDown(void) {
// // clean stuff up here
// }

int readPinMode(uint16_t pin) {
  if (pin >= NUM_DIGITAL_PINS)
    return (-1);

  uint8_t bit = digitalPinToBitMask(pin);
  uint8_t port = digitalPinToPort(pin);
  volatile uint32_t *reg = portModeRegister(port);
  if (*reg & bit)
    return (OUTPUT);

  volatile uint32_t *out = portOutputRegister(port);
  return ((*out & bit) ? INPUT_PULLUP : INPUT);
}

void test_pin_setup(void) {
  setupPinModes();

  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::LeftForward)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::LeftBackward)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::RightForward)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::RightBackward)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::Vacuum)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::LedBlue)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::LedRed)), OUTPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::Speaker)), OUTPUT);

  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::LeftForward)), LOW);
  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::LeftBackward)), LOW);
  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::RightForward)), LOW);
  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::RightBackward)), LOW);
  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::Vacuum)), LOW);
  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::LedBlue)), LOW);
  TEST_ASSERT_EQUAL(digitalRead(as_int(Pins::LedRed)), LOW);

  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::AbyssLeft)), INPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::AbyssCenter)), INPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::AbyssRight)), INPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::VacuumHole)), INPUT);
  TEST_ASSERT_EQUAL(readPinMode(as_int(Pins::Bumper)), INPUT_PULLUP);
}

const long FORWARD_DRIVE_TIME = 1000;
long forwardStartTime, endTime;

void test_forward_as_expected(void) {
  if (millis() - 10 > endTime) {
    TEST_ASSERT_FALSE_MESSAGE(forward(FORWARD_DRIVE_TIME, forwardStartTime),
                              "should have finished by now");
    UNITY_END();
  } else {
    forward(FORWARD_DRIVE_TIME, forwardStartTime);
  }
}

void setup() {
  // NOTE!!! Wait for >2 secs
  // if board doesn't support software reset via Serial.DTR/RTS
  delay(2000);

  UNITY_BEGIN(); // IMPORTANT LINE!
  RUN_TEST(test_pin_setup);
  forwardStartTime = millis();
  endTime = forwardStartTime + FORWARD_DRIVE_TIME;
}

void loop() {
  RUN_TEST(test_forward_as_expected);
  delay(FORWARD_DRIVE_TIME / 20);
}