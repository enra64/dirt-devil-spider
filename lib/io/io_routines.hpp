#include <Arduino.h>

#ifndef IO_ROUTINES_HPP
#define IO_ROUTINES_HPP

enum class Pins {
  LeftForward = 0,
  LeftBackward = 2,
  RightForward = 4,
  RightBackward = 5,

  Vacuum = 34,

  AbyssLeft = 19,
  AbyssRight = 21,
  AbyssCenter = 22,

  VacuumHole = 23,

  Bumper = 13,

  Speaker = 12,

  LedRed = 14,
  LedBlue = 27
};

struct AbyssSensorState {
  bool left, center, right;

  bool triggered() { return left || center || right; }

  AbyssSensorState(bool left, bool center, bool right)
      : left(left), center(center), right(right) {}

  AbyssSensorState() : left(false), center(false), right(false) {}
};

template <typename Pins>
auto as_int(Pins const value) -> typename std::underlying_type<Pins>::type {
  return static_cast<typename std::underlying_type<Pins>::type>(value);
}

void leftForward(float power = 1.);

void rightForward(float power = 1.);

void drive(float left, float right);

bool turn(bool right, long duration, long startTime);
bool turnBackwards(bool right, long duration, long startTime);
bool forward(long duration, long startTime);
bool stop(long duration, long startTime);
bool reverse(long duration, long startTime);

void vacuum(bool enable);

void readAbyssSensors(AbyssSensorState &abyssSensorState);

bool isVacuumHoleClear();

bool isBumperDepressed();

void setRedLed(bool enable);

void setBlueLed(bool enable);

void setSpeaker(bool enable);

void setupPinModes();

#endif